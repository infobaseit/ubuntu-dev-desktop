#!/bin/bash
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get update
sudo apt-get -y install ansible git
git clone https://bueno_infobase@bitbucket.org/infobaseit/ubuntu-dev-desktop.git
cd ubuntu-dev-desktop
ansible-playbook main.yml --connection=local --ask-sudo-pass

